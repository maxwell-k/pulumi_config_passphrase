# Get `PULUMI_CONFIG_PASSPHRASE` for the current project

- Reads `~/.python-gitlab.cfg` to get default GitLab instance
- Gets the project for the current working directory
- Queries the value of the CI variable `PULUMI_CONFIG_PASSPHRASE` for that
  project

Install with [pipx]:

    pipx install git+https://gitlab.com/maxwell-k/pulumi_config_passphrase.git

Usage:

    pulumi_config_passphrase

## Development

Set up a Python virtual environment using [nox]:

    nox

[nox]: https://nox.thea.codes/en/stable/
[pipx]: https://pypa.github.io/pipx/
