from pathlib import Path
from shutil import rmtree

import nox

VIRTUAL_ENVIRONMENT = ".venv"
PYTHON = Path(VIRTUAL_ENVIRONMENT).absolute() / "bin" / "python"
PYTHON_VERSION = "3.11"


@nox.session(python=False)
def venv(session) -> None:
    """Set up a virtual environment

    Uses using pre-built wheels from devpi on DEVPI_INDEX_URL if
    they are available otherwise falls back to WHEELHOUSE and
    ./constraints.txt.
    """
    rmtree(VIRTUAL_ENVIRONMENT, ignore_errors=True)
    session.run(
        f"python{PYTHON_VERSION}",
        "-m",
        "venv",
        "--upgrade-deps",
        VIRTUAL_ENVIRONMENT,
    )
    session.run(
        PYTHON,
        "-m",
        "pip",
        "install",
        "--use-pep517",
        "black",
        "flake8",
        "flit",
        "nox",
        "python-gitlab",
        "reorder-python-imports",
    )
