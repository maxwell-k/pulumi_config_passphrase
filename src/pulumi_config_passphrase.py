"""Retrieve PULUMI_CONFIG_PASSPHRASE from the current project"""
from pathlib import Path
from subprocess import run

from gitlab import Gitlab

__version__ = "2022.11.7"

CONFIG = Path("~/.python-gitlab.cfg").expanduser()

OUTPUT = "export 'PULUMI_CONFIG_PASSPHRASE={}'"


def _project() -> str:
    """Parse the current GitLab project from the url for origin"""
    result = run(
        ["git", "-C", ".pulumi", "remote", "get-url", "origin"],
        capture_output=True,
    )
    result.check_returncode()
    return result.stdout.decode().strip().removesuffix(".git").split(":")[1]


def main() -> int:
    gl = Gitlab.from_config(None, [str(CONFIG)])
    project = gl.projects.get(_project())
    value = project.variables.get("PULUMI_CONFIG_PASSPHRASE").value
    print(OUTPUT.format(value))

    return 0


if __name__ == "__main__":
    raise SystemExit(main())
